//this programm will put out a PPM signal

//////////////////////CONFIGURATION///////////////////////////////
#define chanel_number 1//8  //set the number of chanels
#define default_servo_value 1500  //set the default servo value
#define PPM_FrLen 11000//22500  //set the PPM frame length in microseconds (1ms = 1000µs)
//Spektrum claims 11ms frame rate for 8 channels
#define PPM_PulseLen 900//300  //set the pulse length
//Document says pulse lengths between 0.9 and 2.1ms... 
#define onState 0//1  //set polarity of the pulses: 1 is positive, 0 is negative
//Output may be inverted
#define sigPin 3  //set PPM signal output pin on the arduino
#define selectPin 10 //A && B input select
#define strobePin 2 //
//////////////////////////////////////////////////////////////////


/*this array holds the servo values for the ppm signal
 change theese values in your code (usually servo values move between 1000 and 2000)*/
int ppm[chanel_number];

void setup(){  
  //initiallize default ppm values
  for(int i=0; i<chanel_number; i++){
    ppm[i]= default_servo_value;
  }

  //pinMode(strobePin, OUTPUT);
  //digitalWrite(strobePin, 0);
  pinMode(selectPin, OUTPUT);
  digitalWrite(selectPin, 0);
  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin, !onState);  //set the PPM signal pin to the default state (off)
  
  cli(); //Disables global interrupts. Millis timer & serial comm will be effected
  // Timer/Counter Control register is used to set the timer mode, prescaler and other options.
  // TCCR1 = 0 Stops the timer. So we're setting 2 timers to 0, stopping them
  TCCR1A = 0; // set entire TCCR1 register to 0 
  TCCR1B = 0;
  //Output Compare register can be used to generate an Interrupt after the number of clock ticks written to it. 
  // It is permanently compared to TCNT1. When both match, the compare match interrupt is triggered.
  // 0CR1A = 100, 100 writes before interrupt
  OCR1A = 100;  // compare match register, change this
  // WGM3:0 bits control the counting sequence of the counter 
  TCCR1B |= (1 << WGM12);  // turn on CTC mode (CLear timer on compare)
  // CS12:0 selects clock source to be used
  // Only CS11 as 1 would be Clk / 8 from prescaler 
  TCCR1B |= (1 << CS11);  // 8 prescaler: 0,5 microseconds at 16mhz
  // TIMSK1 = Timer/Counter1 Interrupt Mask Register
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei(); //Enable interrupts 
}

  int counter = 1;
  bool selState = 0;

void loop(){
  //put main code here
  static int val = 10;
  
  ppm[0] = ppm[0] + val;
  if(ppm[0] >= 2150){ val = -10; }
  if(ppm[0] <= 1150){ val = 10; }
  
  delay(100);
  counter += 1;

  if(counter % 100 == 0)
  {
    //digitalWrite(selectPin, !selState);
    counter = 1;
    // Portb Maps to digital Arduino pins 8-13
    // Toggle pin 10
    PORTB ^= 100; //
  }
}

ISR(TIMER1_COMPA_vect){ 
  static boolean state = true;
  //? TCNT
  TCNT1 = 0;
  
  if(state) {  //start pulse
    digitalWrite(sigPin, onState);
    OCR1A = PPM_PulseLen * 2;
    state = false;
  }
  else{  //end pulse and calculate when to start the next pulse
    static byte cur_chan_numb;
    static unsigned int calc_rest;
  
    digitalWrite(sigPin, !onState);
    state = true;

    if(cur_chan_numb >= chanel_number){
      cur_chan_numb = 0;
      calc_rest = calc_rest + PPM_PulseLen;// 
      OCR1A = (PPM_FrLen - calc_rest) * 2;
      calc_rest = 0;
    }
    else{
      OCR1A = (ppm[cur_chan_numb] - PPM_PulseLen) * 2;
      calc_rest = calc_rest + ppm[cur_chan_numb];
      cur_chan_numb++;
    }     
  }
}
