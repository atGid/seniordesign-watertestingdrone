# PPM SIGNALS 

Default Signal Flow:
    Tx -> Rx -> Naza M Lite
    
Modified System
    Tx -> Rx -> ATmega 328P -> Naza M Lite
    
Thorough testing will be done before a full flight test with the new system

# 
**The following forums and sites were helpful in finding more information about the ppm signals**

* [Autonomous Drone](https://autonomousdrone.wordpress.com/2017/02/23/blog-post-title-2/)
* [RC Groups - PPM Signal Reader & Generator](https://www.rcgroups.com/forums/showthread.php?1808432-Arduino-ppm-signal-reader-and-generator)

* http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf

> http://www.avrbeginners.net/architecture/timers/timers.html

> https://mil.ufl.edu/5666/handouts/AVR_PWM_Reg_Info_Summary.pdf
