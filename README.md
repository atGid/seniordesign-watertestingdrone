# Water Testing Drone

EGR 488 - Senior Design

To run the program, attach all components as necessary and upload the WSDV1.ino file to an Arduino Uno or Mega(change the port numbers).
The program will detect a certain change in an analog reading (from the tension sensor) and command the throttle of the drone for the duration of the water collection.
Expect the drone to take approx. 1 minute to gather all data from a location and store it on the SD card in a csv format.

To communicate with the DJI Flight controller, we are generating PPM signals as if they were being sent from the reciever. More information on that is located in the PPM Signals folder
>  https://gitlab.com/atGid/seniordesign-watersamplingdrone/blob/master/PPM%20Signals

To take measurements from the probes we're using the Whitebox Labs tentacle shield which allows from up to 4 circuits(probes) per shield
> https://gitlab.com/atGid/seniordesign-watersamplingdrone/tree/master/Tentacle%20Shield#whats-the-tentacle-shield

Upon completion of data collection, there will be a CSV file containing all data points. For easy visibilty, we want to convert this data file into a KML file that can be viewed in Google Earth.
To do so, the user needs to run the given python script *csvTokml.py* on the created .csv file. This script will format and create a new file of the same name with type KML.