# Whitebox Labs Tentacle Shield for Arduino
`Models T1.15A, T1.16`
## What's the Tentacle Shield?
An Arduino shield to host and individually isolate up to 4 EZO Circuits from Atlas Scientific to measure PH, Dissolved Oxygen, Electric Conductivity (E.C.), Oxidation-Reduction Potential (ORP) and RTD (Temperature).

* 4 channels
* 5V only

Individually isolated measurement circuits and communication lines prevent noise and ground loops for precise measurements even in closed-loop systems.
![Tentacle Shield](https://www.whiteboxes.ch/wp-content/uploads/2015/06/medium_schraeg_schatten1.jpg)



## Getting Started
* [Documentation Site](https://www.whiteboxes.ch/docs/tentacle/t1)
* Download the [Setup Sketch](https://raw.githubusercontent.com/whitebox-labs/tentacle-examples/master/arduino/tentacle-setup/tentacle_setup/tentacle_setup.ino)
* [Quick Start Tutorial](https://www.whiteboxes.ch/docs/tentacle/t1/#/quickstart)

## Code Examples
* [Continuous readings](https://www.whiteboxes.ch/docs/tentacle/t1/#/continuous-example)
* [Continuous, asynchronous readings](https://www.whiteboxes.ch/docs/tentacle/t1/#/asynchronous-example)
* [Interactive, bidirectional communications](https://www.whiteboxes.ch/docs/tentacle/t1/#/interactive-example)
* [Examples Git Repository](https://github.com/whitebox-labs/tentacle-examples) `git clone https://github.com/whitebox-labs/tentacle-examples.git`
* [Download all examples (zip)](https://github.com/whitebox-labs/tentacle-examples/archive/master.zip)

## Downloads
* [Tentacle Setup sketch](https://raw.githubusercontent.com/whitebox-labs/tentacle-examples/master/arduino/tentacle-setup/tentacle_setup/tentacle_setup.ino)
* [T1.16 3D-Model (STL)](https://github.com/whitebox-labs/tentacle/blob/master/hardware/mechanical/tentacle_t1.STL)
* [T1.16 3D-Model (STEP)](https://github.com/whitebox-labs/tentacle/blob/master/hardware/mechanical/tentacle_t1.STEP)
* [T1.16 Mechanical Drawing (PDF)](https://github.com/whitebox-labs/tentacle/raw/master/hardware/mechanical/tentacle_t1_mechanical.pdf)
* [BNC Connector Mechanical Drawing (PDF)](https://github.com/whitebox-labs/tentacle/raw/master/hardware/mechanical/bnc_mechanical.pdf)
* [T1.16 Schematic (PDF)](https://github.com/whitebox-labs/tentacle/raw/master/hardware/tentacle_schematic.pdf)

For more info, visit
> https://github.com/whitebox-labs/tentacle

> https://github.com/whitebox-labs/tentacle-examples