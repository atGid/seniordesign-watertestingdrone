/* Gideon Ojo
 * Mercer University 2019 - Water Testing Drone Project
 * This program is written for an Arduino to read probe data from the Atlas Scientific
 *    probe shield, and generate PPM signals for the flight navigator. All collected 
 *    flight data will be stored on an SD card connceted to to our GPS data logger.
 *    
 * For more information please contact Dr. Anthony Choi or visit the following site
 *    https://gitlab.com/atGid/seniordesign-watertestingdrone 
 */
#include<SPI.h>               //Load SPI Library
#include <SD.h>               //Load SD card library
#include <SoftwareSerial.h>
#include <Adafruit_GPS.h>     //Load the adafruit GPS library
#include <Wire.h>             // enable I2C.

/*PPM*/
#define chanel_number 1           //set the number of chanels
#define default_servo_value 1300  //set the default servo value
#define PPM_FrLen 11000           //set the PPM frame length in microseconds (1ms = 1000µs)
      //Spektrum documentation says 11ms frame rate for 8 channels
#define PPM_PulseLen 900          //set the pulse length
      //Document says pulse lengths between 0.9 and 2.1ms... 
#define onState 0                 //set polarity of the pulses: 1 is positive, 0 is negative
#define sigPin 9                  //set PPM signal output pin on the arduino
#define selectA 2                 //MUX Select Pin
int ppm[chanel_number];           //Initialize Array

#define TOTAL_CIRCUITS 4      //Number of circuits in tentacle shield
#define numReads 2            //Number of times probes are read per location
#define ledPin 13
#define tensionCutoff 575     //Tension Threshold variable
#define chipSelect 10         //chipSelect pin for the SD card Reader


/*Initialize Variables*/
SoftwareSerial mySerial(8,7);         //Initialize the Software Serial port
Adafruit_GPS GPS(&mySerial);          //Create the GPS Object

File mySensorData;                    //Data object you will write your sesnor data to
char sensordata[30];                  // A 30 byte character array to hold incoming data from the sensors
byte sensor_bytes_received = 0;       // We need to know how many characters bytes have been received
byte code = 0;                        // used to hold the I2C response code.
byte in_char = 0;                     // used as a 1 byte buffer to store in bound bytes from the I2C Circuit.
const int channel_ids[] = {99, 102, 97, 100};   //I2C addresses for each probes
float probeValues[] = {0, 0, 0, 0};             //Initialize Array to hold probe readings
const char channel0 PROGMEM = "pH";
const char channel1 PROGMEM = "TMP";
const char channel2 PROGMEM = "DO";
const char channel3 PROGMEM = "CND";
const char * const channel_names[] PROGMEM = {channel0, channel1, channel2, channel3};//{"pH", "TMP", "DO", "CND"}; 

char c;

//Tension Values
//TODO: Refactor naming
const short vals = 10;      //The number of analog voltage readings to average
int tension[vals];          //Initialize array of these readings
short count = 0;            //Counter to store location in array
short sum = 0;              //Store the average of all the readings

/*SETUP*/
void setup(){  
  //initiallize default ppm values
  for(int i=0; i<chanel_number; i++){
    ppm[i]= default_servo_value;
  }
  //Initialize tension values at 0
  for(int i = 0; i < vals; i++) {
    tension[i] = 0;
  }
  //Set MUX selectA as Output and write as 1 (Tx value)
  pinMode(selectA, OUTPUT);
  digitalWrite(selectA, 1);
  //Set PPM pin as output
  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin, !onState);  //set the PPM signal pin to the default state (off)
  
  cli(); //Disables global interrupts. Millis timer & serial comm will be effected
  // Timer/Counter Control register is used to set the timer mode, prescaler and other options.
  // TCCR1 = 0 Stops the timer. 
  TCCR1A = 0; // set entire TCCR1 register to 0 
  TCCR1B = 0;
  //Output Compare register can be used to generate an Interrupt after the number of clock ticks written to it. 
  // It is permanently compared to TCNT1. When both match, the compare match interrupt is triggered.
  OCR1A = 100;  // compare match register, 100 writes before interrupt
  // WGM3:0 bits control the counting sequence of the counter 
  TCCR1B |= (1 << WGM12);  // turn on CTC mode (CLear timer on compare)
  // CS12:0 selects clock source to be used
  TCCR1B |= (1 << CS11);  // 8 prescaler: 0,5 microseconds at 16mhz
  // TIMSK1 = Timer/Counter1 Interrupt Mask Register
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei(); //Enable interrupts 
  
  Serial.begin(9600);
  /*while(!Serial) {
    ;
  } */
  
  delay(1000);
  Wire.begin();                     // Enable I2C
  delay(1000);
  intro();                         // display startup message
  //Setup GPS & SD
  Serial.println(F("Initialize SD Card"));
  pinMode(10, OUTPUT);            //Must declare 10 an output and reserve it to keep SD card happy
  if(!SD.begin(chipSelect))       //Initialize the SD card reader
  {
    Serial.println(F("Init. failed"));
  }
  delay(1000);
  //GPS uses Timer1(also used for PPM) so high baud is needed when not writing
  GPS.begin(115200); //Turn on GPS at 9600 baud
  delay(2000);
  //GPS.sendCommand("$PGCMD,33,0*6D");  //Turn off antenna update nuisance data
  //This breaks SD write. No idea why
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); //Request RMC and GGA Sentences only
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); //Set update rate to 1 hz
  delay(1000); 
  
  //Write new CSV file for probe data
  if(!SD.exists("WTDData.csv")) { 
    addFileHeader();
  }
  else {
    SD.remove("WTDData.csv");
    addFileHeader();
  }
  delay(3000);

  //Upon start of program, Drone should be on the ground
  // causing little to no tension on sensor.
  for(int i = 0; i < vals; i++) {
    updateTension();
  }
  //Until the drone is lifted and the tension value read is past the cutoff
  // wait to start the program
 /* while(sum < tensionCutoff) {
    updateTension();
    Serial.println(sum);
  } */
  Serial.println(F("Program Begins. Delay 5 Sec before reading"));
  delay(5000);
}

  
short counter = 180;
bool isCollecting = false;
short numCollections = 0;
  
void loop(){
  //if(numCollections == 2)
    {
      //Stop taking collections, Leave throttle control to user
    //  manualMode();
    }
    
  updateTension();
 /* if(sum < tensionCutoff) {
    ppm[0] = default_servo_value;
    digitalWrite(selectA, 0);
    Serial.println(F("Collecting = true; selectA =0\n"));
    isCollecting = true;
  }*/
  if(isCollecting)
  {
    //Drone should be stabilized. Read values from all probes
    beginCollection();
    //Lift the drone away from the water before writing information to SD Card
    liftDrone();
    isCollecting = false;
    //Set pin 2 to high to allow user Tx control
    //digitalWrite(selectA, 1);
    PORTD ^=100;  //Directly write pin 2 to 1
    Serial.println(F("Collecting = false; selectA =1\n"));
    counter = 0;
    //Make GPS 9600 baud so you can properly parse for data
    GPS.begin(9600);
    //Write data to SD Card
    writeData();
    GPS.begin(115200);
    Serial.println(F("Delay to allow user flight"));
    delay(5000);
    numCollections++;
  }
  else
  {
    //Do regular work and calculations (check tension)
    updateTension();
  }
  if(counter == 200)
  {
    ppm[0] = 1300;
    //Set pin 2 to low
    //digitalWrite(selectA, 0);
    PORTD &= 011;
    isCollecting = true;
    Serial.println(F("Collecting = true; selectA =0\n"));
    counter = 0;
  }
  counter++; 
  //delay(50);
}

void manualMode() {
  digitalWrite(selectA, 1);
  while(1) {}
}

void updateTension() {
  //Read the tension value and add it to the sum if the number is within range
  if(count == vals) {count = 0;}
  int num = analogRead(A0);
  if(num <= 999) {
    tension[count] = num;
    count += 1;
    sum = 0;
    for(int i = 0; i < vals; i++) {
      sum += tension[i];
    }
    sum = sum / vals;
  }
  delay(50);
}

ISR(TIMER1_COMPA_vect){ 
  static boolean state = true;
  TCNT1 = 0;
  
  if(state) {  //start pulse
    //digitalWrite(sigPin, onState);
    //Write 0 to pin 9
    PORTB &= 01;
    OCR1A = PPM_PulseLen * 2;
    state = false;
  }
  else{  //end pulse and calculate when to start the next pulse
    static byte cur_chan_numb;
    static unsigned int calc_rest;
  
    //digitalWrite(sigPin, !onState);
    //Write 1 to Pin 9
    PORTB ^= 10;
    state = true;

    if(cur_chan_numb >= chanel_number){
      cur_chan_numb = 0;
      calc_rest = calc_rest + PPM_PulseLen;
      OCR1A = (PPM_FrLen - calc_rest) * 2;
      calc_rest = 0;
    }
    else{
      OCR1A = (ppm[cur_chan_numb] - PPM_PulseLen) * 2;
      calc_rest = calc_rest + ppm[cur_chan_numb];
      cur_chan_numb++;
    }     
  }
}

void writeData()
{
    Serial.println(F("Begin Writing"));
    clearGPS();
    mySensorData = SD.open("WTDData.csv", FILE_WRITE); 
    if(!mySensorData) {
      Serial.println(F("Failed to open file"));
      mySensorData.close();
      return; 
    }
    Serial.println(F("File opened"));
    if(GPS.fix) {
      //Write Longitude
      float degWhole=float(int(GPS.longitude/100));     //gives the whole degree part of Longitude
      float degDec = (GPS.longitude - degWhole*100)/60; //give fractional part of longitude
      float deg = degWhole + degDec;                    //Gives complete correct decimal form of Longitude degrees
      if (GPS.lon=='W') {                               //If you are in Western Hemisphere, longitude degrees should be negative
        deg= (-1)*deg;
      }
      mySensorData.print(deg,4);                       //writing decimal degree longitude value to SD card
      mySensorData.print(",");                         //write comma to SD card
      Serial.print(deg,5);      Serial.print(",");
      
      //Write Latitude
      degWhole=float(int(GPS.latitude/100));            //gives the whole degree part of latitude
      degDec = (GPS.latitude - degWhole*100)/60;        //give fractional part of latitude
      deg = degWhole + degDec;                          //Gives complete correct decimal form of latitude degrees
      if (GPS.lat=='S') {                               //If you are in Southern hemisphere latitude should be negative
        deg= (-1)*deg;
      }
      mySensorData.print(deg,4);                        //writing decimal degree longitude value to SD card
      mySensorData.print(",");                          //write comma to SD card
      Serial.print(deg,5);      Serial.print(",");
      
      //Write Altitude
      mySensorData.print(GPS.altitude);      
      mySensorData.print(",");
      Serial.print(GPS.altitude);      Serial.print(",");            
    }
    else {
      mySensorData.print(",,,");
      Serial.print(",,,");
    }
    //Write Date
    //Formatting???
    String s = String(GPS.day) + "/" + String(GPS.month) + "/" + String(GPS.year);
    mySensorData.print(s);    mySensorData.print(",");
    Serial.print(s);    Serial.print(",");
    
    //Write Time
    s = String(GPS.hour) + ":" + String(GPS.minute) + ":" + String(GPS.seconds);
    mySensorData.print(s);    mySensorData.print(",");
    Serial.print(s);    Serial.print(",");
    
    //Add probe data
    mySensorData.print(probeValues[0] / numReads);    Serial.print(probeValues[0] / numReads);
    mySensorData.print(",");    Serial.print(",");
    mySensorData.print(probeValues[1] / numReads);    Serial.print(probeValues[1] / numReads);
    mySensorData.print(",");    Serial.print(",");
    mySensorData.print(probeValues[2] / numReads);    Serial.print(probeValues[2] / numReads);
    mySensorData.print(",");    Serial.print(",");
    mySensorData.print(probeValues[3] / numReads);    Serial.print(probeValues[3] / numReads);
    mySensorData.println();    Serial.println();

    mySensorData.close();
    Serial.println(F("File Closed"));
}

//Approx 25 seconds to stabilize probes and get a 3 reading average
void beginCollection()
{
  Serial.println(F("10 Sec Delay"));
  delay(10000); //10 sec to let probes stabilize
  Serial.println(F("Beg. Collection"));
  for(int i = 0; i < numReads; i++) {
    do_sensor_readings();
  }
}


// take sensor readings continously
void do_sensor_readings() {
  for (int channel = 0; channel < TOTAL_CIRCUITS; channel++) {       // loop through all the sensors
  
    Wire.beginTransmission(channel_ids[channel]);     // call the circuit by its ID number.
    Wire.write('r');                          // request a reading by sending 'r'
    Wire.endTransmission();                         // end the I2C data transmission.
    
    delay(1000);  // AS circuits need a 1 second before the reading is ready

    sensor_bytes_received = 0;                        // reset data counter
    memset(sensordata, 0, sizeof(sensordata));        // clear sensordata array;

    Wire.requestFrom(channel_ids[channel], 48, 1);    // call the circuit and request 48 bytes (this is more then we need).
    code = Wire.read();

    while (Wire.available()) {          // are there bytes to receive?
      in_char = Wire.read();            // receive a byte.

      if (in_char == 0) {               // null character indicates end of command
        Wire.endTransmission();         // end the I2C data transmission.
        break;                          // exit the while loop, we're done here
      }
      else {
        sensordata[sensor_bytes_received] = in_char;      // append this byte to the sensor data array.
        sensor_bytes_received++;
      }
    }
    if(code == 1) {
      if(probeValues[channel] = -1.111) {probeValues[channel] = 0; }
      probeValues[channel] = (probeValues[channel] + atof(sensordata));
    }
    //numReads++;
    /*
    Serial.print(channel_names[channel]);   // print channel name
    Serial.print(':');

    switch (code) {                       // switch case based on what the response code is.
      case 1:                             // decimal 1  means the command was successful.
        Serial.println(sensordata);       // print the actual reading
        break;                              // exits the switch case.

      case 2:                             // decimal 2 means the command has failed.
        Serial.println("command failed");   // print the error
        break;                              // exits the switch case.

      case 254:                           // decimal 254  means the command has not yet been finished calculating.
        Serial.println("circuit not ready"); // print the error
        break;                              // exits the switch case.

      case 255:                           // decimal 255 means there is no further data to send.
        Serial.println("no data");          // print the error
        break;                              // exits the switch case.
    }
    */
  } // for loop
}

void liftDrone()
{
  Serial.println(F("Lift Drone..."));
  ppm[0] = default_servo_value;
  for(short i = 0; i < 200; i++)
  {
    if(i < 100)
      ppm[0] = ppm[0] - 1;
    else
      ppm[0] = ppm[0] + 1;
    delay(50);
  }

  Serial.println(F("Stabilized"));
}

// Print intro
void intro() {
  Serial.flush();
  Serial.println(" ");
  Serial.println(F("READY_"));
}
void error(uint8_t errno) {
  /*
  if (SD.errorCode()) {
   putstring("SD error: ");
   Serial.print(card.errorCode(), HEX);
   Serial.print(',');
   Serial.println(card.errorData(), HEX);
   }*/
   
  while(1) {
    uint8_t i;
    for (i=0; i<errno; i++) {
      digitalWrite(ledPin, HIGH);
      delay(100);
      digitalWrite(ledPin, LOW);
      delay(100);
    }
    for (i=errno; i<10; i++) {
      delay(200);
    }
  }
}


void clearGPS() 
{  //Clear old and corrupt data from serial port 
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
   while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  
}

//Take GPS readings
void readGPS() 
{
  clearGPS();
  while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  String NMEA1=GPS.lastNMEA();
  
   while(!GPS.newNMEAreceived()) { //Loop until you have a good NMEA sentence
    c=GPS.read();
  }
  GPS.parse(GPS.lastNMEA()); //Parse that last good NMEA sentence
  String NMEA2=GPS.lastNMEA();
  
  Serial.println(NMEA1);
  Serial.println(NMEA2);
  Serial.println("");
}

void addFileHeader() {
  delay(500);
  //mySensorData.close();
  mySensorData = SD.open("WTDData.csv", FILE_WRITE);
  delay(1000);
  if(!mySensorData) {
      //Try again, then if still fails. Quit
      mySensorData.close();
      Serial.println(F("Try to open file again"));
      mySensorData = SD.open("WTDData.csv", FILE_WRITE);
      delay(1000);
    if(!mySensorData) {
      Serial.println(F("Failed to Open File"));
    error(3);
    }
  }
  else {
    mySensorData.println(F("Longitude, Latitude, Altitude, Date, Time, pH, Temperature, Dissolved Oxygen, Conductivity"));
    mySensorData.close();
    Serial.println(F("Header Added"));
  }
}
